package com.example.artem.saxonysocial.insert_item_handlers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.artem.saxonysocial.Constants;
import com.example.artem.saxonysocial.R;
import com.example.artem.saxonysocial.RequestHandler;
import com.example.artem.saxonysocial.SharedPrefManager;
import com.example.artem.saxonysocial.activities_main.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AusfallgrundAddActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_ausfallgrund_type;
    private Button btn_submit;
    private ProgressDialog progressDialog;

    final String table_name = "ausfallgrund";
    final String field_name = "ausfallgrund_type";
    final String id_field = "ausfallgrund_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ausfallgrund_add);
        // Login validation
        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        et_ausfallgrund_type = (EditText) findViewById(R.id.et_ausfallgrund_type);
        btn_submit = (Button) findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);

    }

    private void submitRecord(){

        final String ausfallgrund_type = et_ausfallgrund_type.getText().toString().trim();

        if (TextUtils.isEmpty(ausfallgrund_type)) {
            et_ausfallgrund_type.setError("Please enter ausfallgrund type");
            et_ausfallgrund_type.requestFocus();
            return;
        }

        progressDialog.setMessage("Adding new ausfallgrund type...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_ADD_ITEM,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("table_name", table_name);
                params.put("field_name", field_name);
                params.put("id_field", id_field);
                params.put("item_name", ausfallgrund_type);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
        et_ausfallgrund_type.setText("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menuLogout:
                SharedPrefManager.getInstance(this).logout();
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.menuSettings:
                Toast.makeText(this, "You clicked settings", Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if(v == btn_submit)
            submitRecord();
    }
}
