package com.example.artem.saxonysocial.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.artem.saxonysocial.R;
import com.example.artem.saxonysocial.helper_classes.DataSet;

import java.util.List;

/**
 * Created by artem on 04.02.18.
 */

public class GruppenarbeitRecordsListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<DataSet> DataList;

    public GruppenarbeitRecordsListAdapter(Activity activity, List<DataSet> dataitem) {
        this.activity = activity;
        this.DataList = dataitem;
    }

    @Override
    public int getCount() {
        return DataList.size();
    }

    @Override
    public Object getItem(int position) {
        return DataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_item_gruppenarbeit_overall, null);

        TextView record_id = (TextView) convertView.findViewById(R.id.record_id_lt);
        TextView datum = (TextView) convertView.findViewById(R.id.datum_lt);
        TextView dauer_in_h = (TextView) convertView.findViewById(R.id.dauer_in_h_lt);

        DataSet m = DataList.get(position);
        record_id.setText(m.getRecordId());
        datum.setText(m.getDatum());
        dauer_in_h.setText(m.getDauer_in_h());

        return convertView;
    }

}
