package com.example.artem.saxonysocial;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.artem.saxonysocial.activities_main.LoginActivity;
import com.example.artem.saxonysocial.insert_item_handlers.WetterAddActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class StreetworkActivityProto extends AppCompatActivity implements View.OnClickListener {

    private EditText et_n_u12_m, et_n_u12_w, et_n_u14_m, et_n_u14_w, et_anmerkungen,
            et_datum, et_dauer_in_h;
    private Button btn_revert, btn_submit, btn_new_record;
    private CheckBox th_sucht, th_gewalt, th_gesundheit, th_identitat,
            th_freizeit, th_kriminalitat;

    private final String TABLE_NAME = "streetwork_record";
    private final String FIELD_NAME = "streetwork_record_id";

    private ProgressDialog progressDialog;

    private JSONArray result;
    private Spinner spinnerUsers;
    private ArrayList<String> usersList;

    private JSONArray resultWetter;
    private Spinner spinnerWetter;
    private ArrayList<String> wetterList;

    @Override
    protected void onRestart() {
        super.onRestart();
        fetchUsers();
        fetchWetter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streetwork_proto);

        // Login validation
        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }


        et_datum = (EditText) findViewById(R.id.et_datum);
        et_dauer_in_h = (EditText) findViewById(R.id.et_uhrzeit);

        et_n_u12_m = (EditText) findViewById(R.id.et_n_bekannt);
        et_n_u12_w = (EditText) findViewById(R.id.et_n_unbekannt);
        et_n_u14_m = (EditText) findViewById(R.id.et_n_u12_m);
        et_n_u14_w = (EditText) findViewById(R.id.et_n_u12_w);
        et_anmerkungen = (EditText) findViewById(R.id.et_anmerkungen);

        th_sucht = (CheckBox) findViewById(R.id.th_sucht);
        th_gewalt = (CheckBox) findViewById(R.id.th_wohnraum);
        th_gesundheit = (CheckBox) findViewById(R.id.th_familie);
        th_identitat = (CheckBox) findViewById(R.id.th_finanzielle);
        th_freizeit = (CheckBox) findViewById(R.id.th_gewalt);
        th_kriminalitat = (CheckBox) findViewById(R.id.th_behorden);

        btn_new_record = (Button) findViewById(R.id.btn_new_record);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_revert = (Button) findViewById(R.id.btn_revert);

        spinnerUsers = (Spinner) findViewById(R.id.spinnerMitarbeiter3);
        usersList = new ArrayList<String>();

        spinnerWetter = (Spinner) findViewById(R.id.spinnerWetter);
        wetterList = new ArrayList<String>();

        SimpleDateFormat dateF = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        SimpleDateFormat timeF = new SimpleDateFormat("HH:mm", Locale.getDefault());

        String date = dateF.format(Calendar.getInstance().getTime());
        String time = timeF.format(Calendar.getInstance().getTime());

        et_datum.setText(date);
        et_dauer_in_h.setText(time);

        progressDialog = new ProgressDialog(this);

        btn_submit.setOnClickListener(this);
        btn_new_record.setOnClickListener(this);
        btn_revert.setOnClickListener(this);

//        Generation of context menus
        registerForContextMenu(spinnerWetter);
        registerForContextMenu(spinnerUsers);

        fetchUsers();
        fetchWetter();

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId()==R.id.spinnerWetter){
            getMenuInflater().inflate(R.menu.menu_spinner_wetter, menu);
        }

        if (v.getId()==R.id.spinnerMitarbeiter3){
            getMenuInflater().inflate(R.menu.menu_spinner_users, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_user:
                showToast("Add user Clicked");
                add_new_user();
                return true;
            case R.id.remove_user:
                showToast("Remove user Clicked");
//                remove_user();
                return true;
            case R.id.add_wetter:
                showToast("Add wetter Clicked");
                add_new_wetter();
                return true;
            case R.id.remove_wetter:
                showToast("Remove wetter Clicked");
                remove_wetter();
                fetchWetter();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void add_new_user(){
        Intent i = new Intent(getApplicationContext(), UserRegisterActivity.class);
        startActivity(i);
    }

    private void add_new_wetter(){
        Intent i = new Intent(getApplicationContext(), WetterAddActivity.class);
        startActivity(i);
    }

    private void remove_wetter(){
        progressDialog.setMessage("Deleting selected wetter type");
        progressDialog.show();

        final String spinnerWetterSelection = spinnerWetter.getSelectedItem().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_ITEM_DELETE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("wetter", spinnerWetterSelection);

                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
//        fetchWetter();
    }

    public void showToast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void fetchUsers() {
        progressDialog.setMessage("Filling the selectors...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                Constants.URL_FETCH_USERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            result = jsonObject.getJSONArray("users");
                            empdetailsUsers(result);
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void empdetailsUsers(JSONArray jsonObject) {

        usersList.clear();

        for (int i = 0; i < jsonObject.length(); i++) {
            try {
                JSONObject json = jsonObject.getJSONObject(i);
                usersList.add(json.getString("username"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // arrayList.add(0,"Select Employee");

        ArrayAdapter<String> usersAdapter = new ArrayAdapter<String>(StreetworkActivityProto.this, android.R.layout.simple_spinner_dropdown_item, usersList);
        usersAdapter.notifyDataSetChanged();
        spinnerUsers.setAdapter(usersAdapter);
//        spinnerUsers.setAdapter(new ArrayAdapter<String>(StreetworkActivityProto.this, android.R.layout.simple_spinner_dropdown_item, usersList));

    }


    private void fetchWetter() {
        progressDialog.setMessage("Filling the selectors...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                Constants.URL_FETCH_WETTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            resultWetter = jsonObject.getJSONArray("wetter");
                            empdetailsWetter(resultWetter);
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void empdetailsWetter(JSONArray jsonObject) {

        wetterList.clear();


        for (int i = 0; i < jsonObject.length(); i++) {
            try {
                JSONObject json = jsonObject.getJSONObject(i);
                wetterList.add(json.getString("wetter_type"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // arrayList.add(0,"Select Employee");
        ArrayAdapter<String> wetterAdapter = new ArrayAdapter<String>(StreetworkActivityProto.this, android.R.layout.simple_spinner_dropdown_item, wetterList);
        wetterAdapter.notifyDataSetChanged();
        spinnerWetter.setAdapter(wetterAdapter);
//        spinnerWetter.setAdapter(new ArrayAdapter<String> (StreetworkActivityProto.this, android.R.layout.simple_spinner_dropdown_item, wetterList));

    }


//    private String getSpinnerUsersSelection(int position){
//        String name="";
//        try {
//            //Getting object of given index
//            JSONObject json = result.getJSONObject(position);
//            //Fetching name from that object
//            name = json.getString("username");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        //Returning the name
//        return name;
//    }


    private void submitRecord() {

        String datum = et_datum.getText().toString().trim();
        String dauer_in_h = et_dauer_in_h.getText().toString().trim();
        String n_u12_m = et_n_u12_m.getText().toString().trim();
        String n_u12_w = et_n_u12_w.getText().toString().trim();
        String n_u14_m = et_n_u14_m.getText().toString().trim();
        String n_u14_w = et_n_u14_w.getText().toString().trim();
        String anmerkungen = et_anmerkungen.getText().toString().trim();
        final String spinnerUsersSelection = spinnerUsers.getSelectedItem().toString().trim();
        final String spinnerWetterSelection = spinnerWetter.getSelectedItem().toString().trim();

        final String mitarbeiter1 = SharedPrefManager.getInstance(this).getUsername();

        final String datum_val, dauer_in_h_val;
        final String th_sucht_val, th_gesundheit_val, th_gewalt_val, th_freizeit_val, th_identitat_val,
                th_kriminalitat_val;

        final String n_u12_m_val, n_u12_w_val, n_u14_m_val, n_u14_w_val, anmerkungen_val;


        if (n_u12_m.matches("")){n_u12_m_val="0";}else{n_u12_m_val=n_u12_m;}
        if (n_u12_w.matches("")){n_u12_w_val="0";}else{n_u12_w_val=n_u12_w;}
        if (n_u14_m.matches("")){n_u14_m_val="0";}else{n_u14_m_val=n_u14_m;}
        if (n_u14_w.matches("")){n_u14_w_val="0";}else{n_u14_w_val=n_u14_w;}
        if (anmerkungen.matches("")){anmerkungen_val="-";}else{anmerkungen_val=anmerkungen;}




        if (th_sucht.isChecked()) {
            th_sucht_val = "1";
        } else {
            th_sucht_val = "0";
        }

        if (th_gewalt.isChecked()) {
            th_gewalt_val = "1";
        } else {
            th_gewalt_val = "0";
        }

        if (th_gesundheit.isChecked()) {
            th_gesundheit_val = "1";
        } else {
            th_gesundheit_val = "0";
        }

        if (th_identitat.isChecked()) {
            th_identitat_val = "1";
        } else {
            th_identitat_val = "0";
        }

        if (th_freizeit.isChecked()) {
            th_freizeit_val = "1";
        } else {
            th_freizeit_val = "0";
        }

        if (th_kriminalitat.isChecked()) {
            th_kriminalitat_val = "1";
        } else {
            th_kriminalitat_val = "0";
        }

        // Validation is not empty
        if (TextUtils.isEmpty(datum)) {
            et_datum.setError("Please enter the date");
            et_datum.requestFocus();
            return;
        }

        if (datum.matches("\\d{2}.\\d{2}.\\d{4}")){
            datum_val = datum;
        }else{
            et_datum.setError("Please check the date format");
            et_datum.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(dauer_in_h)) {
            et_dauer_in_h.setError("Please enter the duration in hours");
            et_dauer_in_h.requestFocus();
            return;
        }

        if (dauer_in_h.matches("\\d{2}:\\d{2}")){
            dauer_in_h_val = dauer_in_h;
        }else{
            et_dauer_in_h.setError("Please check time format");
            et_dauer_in_h.requestFocus();
            return;
        }


        int n_u12_mw_s = Integer.parseInt(n_u12_m_val) + Integer.parseInt(n_u12_w_val);
        int n_u14_mw_s = Integer.parseInt(n_u14_m_val) + Integer.parseInt(n_u14_w_val);

        if (n_u12_mw_s != n_u14_mw_s) {
            et_n_u14_w.setError("Not equal");
            et_n_u14_m.setError("Not equal");
        } else {


            progressDialog.setMessage("Adding new record...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    Constants.URL_ADD_STREETWORK_RECORD,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.hide();
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("datum", datum_val);
                    params.put("dauer_in_h", dauer_in_h_val);
                    params.put("n_u12_m", n_u12_m_val);
                    params.put("n_u12_w", n_u12_w_val);
                    params.put("n_u14_m", n_u14_m_val);
                    params.put("n_u14_w", n_u14_w_val);
                    params.put("th_sucht", th_sucht_val);
                    params.put("th_gewalt", th_gewalt_val);
                    params.put("th_gesundheit", th_gesundheit_val);
                    params.put("th_freizeit", th_freizeit_val);
                    params.put("th_identitat", th_identitat_val);
                    params.put("th_kriminalitat", th_kriminalitat_val);
                    params.put("anmerkungen", anmerkungen_val);
                    params.put("mitarbeiter1", mitarbeiter1);
                    params.put("wetter", spinnerWetterSelection);
                    params.put("mitarbeiter2", spinnerUsersSelection);

                    return params;
                }
            };
            RequestHandler.getInstance(this).addToRequestQueue(stringRequest);


        }
    }

    // Script for clearing fields after clicking "New record button"
    private void clearFields(){
        Toast.makeText(getApplicationContext(),"Clearing fields...", Toast.LENGTH_LONG).show();
        et_n_u12_m.setText("");
        et_n_u12_w.setText("");
        et_n_u14_m.setText("");
        et_n_u14_w.setText("");
        et_anmerkungen.setText("");
        if (th_sucht.isChecked())
            th_sucht.setChecked(false);
        if (th_gewalt.isChecked())
            th_gewalt.setChecked(false);
        if (th_kriminalitat.isChecked())
            th_kriminalitat.setChecked(false);
        if (th_identitat.isChecked())
            th_identitat.setChecked(false);
        if (th_freizeit.isChecked())
            th_freizeit.setChecked(false);
        if (th_gesundheit.isChecked())
            th_gesundheit.setChecked(false);

    }

    @Override
    public void onClick(View view) {
        if(view == btn_submit){
            submitRecord();
        }
        else if(view == btn_new_record){
            clearFields();
        }
        else if(view == btn_revert){
            deleteLastRecord();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menuLogout:
                SharedPrefManager.getInstance(this).logout();
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.menuSettings:
                Toast.makeText(this, "You clicked settings", Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }

    private void deleteLastRecord(){
        progressDialog.setMessage("Deleting last inserted record...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_DELETE_LAST_RECORD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("db_table", TABLE_NAME);
                params.put("db_field", FIELD_NAME);

                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

}
