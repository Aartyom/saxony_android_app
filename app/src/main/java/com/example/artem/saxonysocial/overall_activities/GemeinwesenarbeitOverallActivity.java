package com.example.artem.saxonysocial.overall_activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.artem.saxonysocial.Constants;
import com.example.artem.saxonysocial.R;
import com.example.artem.saxonysocial.SharedPrefManager;
import com.example.artem.saxonysocial.activities_main.LoginActivity;
import com.example.artem.saxonysocial.adapters.GemeinwesenarbeitRecordsListAdapter;
import com.example.artem.saxonysocial.adapters.StreetworkRecordsListAdapter;
import com.example.artem.saxonysocial.helper_classes.DataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GemeinwesenarbeitOverallActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private ListView listView;
    private JSONArray result;

//    Check adapter
    ArrayList<DataSet> gemeinwesenarbeitRecordsList;
    private GemeinwesenarbeitRecordsListAdapter gemeinwesenarbeitRecordsListAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gemeinwesenarbeit_overall);

        listView = (ListView) findViewById(R.id.list);

        gemeinwesenarbeitRecordsList = new ArrayList<DataSet>();
        gemeinwesenarbeitRecordsListAdapter = new GemeinwesenarbeitRecordsListAdapter(this, gemeinwesenarbeitRecordsList);


        listView.setAdapter(gemeinwesenarbeitRecordsListAdapter);

        progressDialog = new ProgressDialog(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        fetchRecords();
    }


    private void fetchRecords() {
        progressDialog.setMessage("Filling the selectors...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                Constants.URL_GEMEINWESENARBEIT_RECORDS_TABLE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            result = jsonObject.getJSONArray("records");
                            /**-------------------------------------------------------------------**/

                            for (int i = 0; i < result.length(); i++) {
                                try {
                                    JSONObject json = result.getJSONObject(i);
                                    DataSet dataSet = new DataSet();
                                    dataSet.setRecordId(json.getString("gemeinwesenarbeit_record_id"));
                                    dataSet.setDatum(json.getString("datum"));
                                    dataSet.setDauer_in_h(json.getString("dauer_in_h"));
                                    dataSet.setPlatz(json.getString("platz"));

                                    Log.e("Error", json.getString("gemeinwesenarbeit_record_id"));
                                    Log.e("Error", json.getString("datum"));
                                    Log.e("Error", json.getString("dauer_in_h"));
                                    Log.e("Error", json.getString("platz"));
                                    gemeinwesenarbeitRecordsList.add(dataSet);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            gemeinwesenarbeitRecordsListAdapter.notifyDataSetChanged();

                            /**-------------------------------------------------------------------**/
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e("Error", "Unable to parse json array");
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menuLogout:
                SharedPrefManager.getInstance(this).logout();
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.menuSettings:
                Toast.makeText(this, "You clicked settings", Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }

}
