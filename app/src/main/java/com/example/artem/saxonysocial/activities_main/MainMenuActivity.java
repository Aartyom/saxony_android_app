package com.example.artem.saxonysocial.activities_main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.artem.saxonysocial.overall_activities.StreetworkOverallActivity;
import com.example.artem.saxonysocial.R;
import com.example.artem.saxonysocial.SharedPrefManager;
import com.example.artem.saxonysocial.overall_activities.EinzelfallhilfeOverallActivity;
import com.example.artem.saxonysocial.overall_activities.GemeinwesenarbeitOverallActivity;
import com.example.artem.saxonysocial.overall_activities.GruppenarbeitOverallActivity;

public class MainMenuActivity extends AppCompatActivity {

    Button btnStreetwork, btnGruppenarbeit, btnGemeinwesenarbeit, btnEinzelfallhilfe, btnStreetworkOverall, btnGruppenarbeitOverall,
            btnEinzelfallhilfeOverall, btnGemeinwesenarbeitOverall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        // Login validation
        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        btnStreetwork = (Button) findViewById(R.id.btnStreetwork);
        btnGruppenarbeit =(Button) findViewById(R.id.btnGruppenarbeit);
        btnGemeinwesenarbeit = (Button) findViewById(R.id.btnGemeinwesenarbeit);
        btnEinzelfallhilfe = (Button) findViewById(R.id.btnEinzelfallhilfe);
        btnStreetworkOverall = (Button) findViewById(R.id.btnStreetworkOverall);
        btnGruppenarbeitOverall = (Button) findViewById(R.id.btnGruppenarbeitOverall);
        btnEinzelfallhilfeOverall = (Button) findViewById(R.id.btnEinzelfallhilfeOverall);
        btnGemeinwesenarbeitOverall = (Button) findViewById(R.id.btnGemeinwesenarbeitOverall);


        btnStreetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), StreetworkActivity.class);
                startActivity(i);
            }
        });


        btnGruppenarbeit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), GruppenarbeitActivity.class);
                startActivity(i);
            }
        });

        btnGemeinwesenarbeit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), GemeinwesenarbeitActivity.class);
                startActivity(i);
            }
        });

        btnEinzelfallhilfe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), EinzelfallHilfeActivity.class);
                startActivity(i);
            }
        });

        btnStreetworkOverall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), StreetworkOverallActivity.class);
                startActivity(i);
            }
        });

        btnGruppenarbeitOverall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), GruppenarbeitOverallActivity.class);
                startActivity(i);
            }
        });

        btnGemeinwesenarbeitOverall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), GemeinwesenarbeitOverallActivity.class);
                startActivity(i);
            }
        });

        btnEinzelfallhilfeOverall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), EinzelfallhilfeOverallActivity.class);
                startActivity(i);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menuLogout:
                SharedPrefManager.getInstance(this).logout();
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.menuSettings:
                Toast.makeText(this, "You clicked settings", Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }
}
