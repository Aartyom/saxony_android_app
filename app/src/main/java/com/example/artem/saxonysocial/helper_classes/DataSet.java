package com.example.artem.saxonysocial.helper_classes;

/**
 * Created by artem on 04.02.18.
 */

public class DataSet {

    private String recordId, datum, uhrzeit, platz, dauer_in_h;

    public String getRecordId() {
        return recordId;
    }

    public String getDauer_in_h() {
        return dauer_in_h;
    }

    public void setDauer_in_h(String dauer_in_h) {
        this.dauer_in_h = dauer_in_h;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getUhrzeit() {
        return uhrzeit;
    }

    public void setUhrzeit(String uhrzeit) {
        this.uhrzeit = uhrzeit;
    }

    public String getPlatz() {
        return platz;
    }

    public void setPlatz(String platz) {
        this.platz = platz;
    }
}
