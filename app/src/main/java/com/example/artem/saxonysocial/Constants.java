package com.example.artem.saxonysocial;

public class Constants {

    private static final String ROOT_URL = "http://10.0.2.2/saxony_social/my/";

    public static final String URL_REGISTER = ROOT_URL+"addUser.php";

    public static final String URL_LOGIN = ROOT_URL+"userLogin1.php";


    public static final String URL_DELETE_LAST_RECORD = ROOT_URL+"deleteLastRecord.php";

    public static final String URL_ADD_GRUPPENARBEIT_RECORD = ROOT_URL+"gruppenarbeitAdd.php";
    public static final String URL_ADD_GEMEINWESENARBEIT_RECORD = ROOT_URL+"gemeinwesenarbeitAdd.php";
    public static final String URL_ADD_EINZELFALLHILFE_RECORD = ROOT_URL+"einzelfallhilfeAdd.php";
    public static final String URL_ADD_STREETWORK_RECORD = ROOT_URL+"streetworkAdd.php";


    //    Get request URLs
    public static final String URL_FETCH_USERS = ROOT_URL+"fetch_handlers/getUsers.php";
    public static final String URL_FETCH_PROJECTS = ROOT_URL+"fetch_handlers/getProjects.php";
    public static final String URL_FETCH_WETTER = ROOT_URL+"fetch_handlers/getWetter.php";
    public static final String URL_FETCH_PLATZ = ROOT_URL+"fetch_handlers/getPlatz.php";
    public static final String URL_FETCH_MOBILITAT = ROOT_URL+"fetch_handlers/getMobilitat.php";
    public static final String URL_FETCH_AUSFALLGRUND = ROOT_URL+"fetch_handlers/getAusfallgrund.php";

    public static final String URL_STREETWORK_RECORDS_TABLE = ROOT_URL+"overall_tables/get_streetwork_records_table.php";
    public static final String URL_GEMEINWESENARBEIT_RECORDS_TABLE = ROOT_URL+"overall_tables/get_gemeinwesenarbeit_records_table.php";
    public static final String URL_GRUPPENARBEIT_RECORDS_TABLE = ROOT_URL+"overall_tables/get_gruppenarbeit_records_table.php";
    public static final String URL_EINZELFALLHILFE_RECORDS_TABLE = ROOT_URL+"overall_tables/get_einzelfallhilfe_records_table.php";

//    public static final String URL_ADD_ITEM = ROOT_URL+"addItem.php";
//    public static final String URL_WETTER_DELETE = ROOT_URL+"wetterDelete.php";
//    public static final String URL_PROJECT_DELETE = ROOT_URL+"projectDelete.php";

    public static final String URL_ITEM_DELETE = ROOT_URL+"deleteItem.php";
    public static final String URL_ADD_ITEM = ROOT_URL+"addItem.php";

}
