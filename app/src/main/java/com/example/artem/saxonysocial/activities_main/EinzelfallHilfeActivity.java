package com.example.artem.saxonysocial.activities_main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.artem.saxonysocial.Constants;
import com.example.artem.saxonysocial.insert_item_handlers.ProjectAddActivity;
import com.example.artem.saxonysocial.R;
import com.example.artem.saxonysocial.RequestHandler;
import com.example.artem.saxonysocial.SharedPrefManager;
import com.example.artem.saxonysocial.UserRegisterActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EinzelfallHilfeActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_n_u12_m, et_n_u12_w, et_n_u14_m, et_n_u14_w, et_n_14_17_m,
            et_n_14_17_w, et_n_18_20_m, et_n_18_20_w, et_n_21_24_m, et_n_21_24_w,
            et_n_25_26_m, et_n_25_26_w, et_n_o27_m, et_n_o27_w, et_anmerkungen, et_sonstiges, et_datum, et_dauer_in_h;

    private CheckBox th_sucht, th_gewalt, th_kriminalitat, th_gesundheit, th_freizeit,
            th_identitat, th_wohnraum, th_familie, th_behorden,
            th_finanzielle, th_arbeit, th_beziehung, th_politische_themen, th_digitales, th_vorstellung;

    private Button btn_revert, btn_submit, btn_new_record;
    private ProgressDialog progressDialog;

    private JSONArray result;
    private JSONArray resultProject;

    private Spinner spinnerProjectName, spinnerMitarbeiter2, spinnerMitarbeiter3;
    private ArrayList<String> usersList;
    private ArrayList<String> projectsList;

    @Override
    protected void onRestart() {
        super.onRestart();
        fetchUsers();
        fetchProjects();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_einzelfall_hilfe);

        // Login validation
        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        et_datum = (EditText) findViewById(R.id.et_datum);
        et_dauer_in_h = (EditText) findViewById(R.id.et_uhrzeit);
//        et_n_bekannt = (EditText) findViewById(R.id.et_n_bekannt);
//        et_n_unbekannt = (EditText) findViewById(R.id.et_n_unbekannt);
        et_n_u12_m = (EditText) findViewById(R.id.et_n_u12_m);
        et_n_u12_w = (EditText) findViewById(R.id.et_n_u12_w);
        et_n_u14_m = (EditText) findViewById(R.id.et_n_u14_m);
        et_n_u14_w = (EditText) findViewById(R.id.et_n_u14_w);
        et_n_14_17_m = (EditText) findViewById(R.id.et_n_14_17_m);
        et_n_14_17_w = (EditText) findViewById(R.id.et_n_14_17_w);
        et_n_18_20_m = (EditText) findViewById(R.id.et_n_18_20_m);
        et_n_18_20_w = (EditText) findViewById(R.id.et_n_18_20_w);
        et_n_21_24_m = (EditText) findViewById(R.id.et_n_21_24_m);
        et_n_21_24_w = (EditText) findViewById(R.id.et_n_21_24_w);
        et_n_25_26_m = (EditText) findViewById(R.id.et_n_25_26_m);
        et_n_25_26_w = (EditText) findViewById(R.id.et_n_25_26_w);
        et_n_o27_m = (EditText) findViewById(R.id.et_n_o27_m);
        et_n_o27_w = (EditText) findViewById(R.id.et_n_o27_w);
        th_sucht = (CheckBox) findViewById(R.id.th_sucht);
        th_gewalt = (CheckBox) findViewById(R.id.th_gewalt);
        th_kriminalitat = (CheckBox) findViewById(R.id.th_kriminalitat);
        th_gesundheit = (CheckBox) findViewById(R.id.th_gesundheit);
        th_freizeit = (CheckBox) findViewById(R.id.th_freizeit);
        th_identitat = (CheckBox) findViewById(R.id.th_identitat);
        th_wohnraum = (CheckBox) findViewById(R.id.th_wohnraum);
        th_familie = (CheckBox) findViewById(R.id.th_familie);
        th_behorden = (CheckBox) findViewById(R.id.th_behorden);
        th_finanzielle = (CheckBox) findViewById(R.id.th_finanzielle);
        th_arbeit = (CheckBox) findViewById(R.id.th_arbeit);
        th_beziehung = (CheckBox) findViewById(R.id.th_beziehung);
        th_politische_themen = (CheckBox) findViewById(R.id.th_politische_themen);
        th_digitales = (CheckBox) findViewById(R.id.th_digitales);
        th_vorstellung = (CheckBox) findViewById(R.id.th_vorstellung);
        et_anmerkungen = (EditText) findViewById(R.id.et_anmerkungen);
        et_sonstiges = (EditText) findViewById(R.id.et_th_sonstiges);
        btn_new_record = (Button) findViewById(R.id.btn_new_record);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_revert = (Button) findViewById(R.id.btn_revert);
        spinnerProjectName = (Spinner) findViewById(R.id.spinnerProjectName);
        spinnerMitarbeiter2 = (Spinner) findViewById(R.id.spinnerMitarbeiter2);
        spinnerMitarbeiter3 = (Spinner) findViewById(R.id.spinnerMitarbeiter3);
        usersList = new ArrayList<String>();
        projectsList = new ArrayList<String>();


        SimpleDateFormat dateF = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

        String date = dateF.format(Calendar.getInstance().getTime());
        et_datum.setText(date);

        et_dauer_in_h.setText("1");

        progressDialog = new ProgressDialog(this);

        btn_submit.setOnClickListener(this);

        registerForContextMenu(spinnerProjectName);

        fetchUsers();
        fetchProjects();
    }

    private void submitRecord(){

        final String th_sucht_val, th_gewalt_val, th_kriminalitat_val, th_gesundheit_val,
                th_freizeit_val, th_identitat_val, th_wohnraum_val, th_familie_val,
                th_behorden_val, th_finanzielle_val, th_arbeit_val, th_beziehung_val,
                th_politische_themen_val, th_digitales_val, th_vorstellung_val;

        final String datum_val, n_u12_m_val, n_u12_w_val, n_u14_m_val, n_u14_w_val, n_14_17_m_val,
                n_14_17_w_val, n_18_20_m_val, n_18_20_w_val, n_21_24_m_val, n_21_24_w_val,
                n_25_26_m_val, n_25_26_w_val, n_o27_m_val, n_o27_w_val, anmerkungen_val, sonstiges_val;

        String datum = et_datum.getText().toString().trim();
        final String dauer_in_h = et_dauer_in_h.getText().toString().trim();
//        String n_bekannt = et_n_bekannt.getText().toString().trim();
//        String n_unbekannt = et_n_unbekannt.getText().toString().trim();
        String n_u12_m = et_n_u12_m.getText().toString().trim();
        String n_u12_w = et_n_u12_w.getText().toString().trim();
        String n_u14_m = et_n_u14_m.getText().toString().trim();
        String n_u14_w = et_n_u14_w.getText().toString().trim();
        String n_14_17_m = et_n_14_17_m.getText().toString().trim();
        String n_14_17_w = et_n_14_17_w.getText().toString().trim();
        String n_18_20_m = et_n_18_20_m.getText().toString().trim();
        String n_18_20_w = et_n_18_20_w.getText().toString().trim();
        String n_21_24_m = et_n_21_24_m.getText().toString().trim();
        String n_21_24_w = et_n_21_24_w.getText().toString().trim();
        String n_25_26_m = et_n_25_26_m.getText().toString().trim();
        String n_25_26_w = et_n_25_26_w.getText().toString().trim();
        String n_o27_m = et_n_o27_m.getText().toString().trim();
        String n_o27_w = et_n_o27_w.getText().toString().trim();
        String anmerkungen = et_anmerkungen.getText().toString().trim();
        String sonstiges = et_sonstiges.getText().toString().trim();
        final String spinnerProjectNameSelection = spinnerProjectName.getSelectedItem().toString().trim();
        final String spinnerMitarbeiter2Selection = spinnerMitarbeiter2.getSelectedItem().toString().trim();
        final String spinnerMitarbeiter3Selection = spinnerMitarbeiter3.getSelectedItem().toString().trim();
        final String mitarbeiter1 = SharedPrefManager.getInstance(this).getUsername();

        if (TextUtils.isEmpty(datum)) {
            et_datum.setError("Please enter the date");
            et_datum.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(dauer_in_h)) {
            et_dauer_in_h.setError("Please enter the duration in hours");
            et_dauer_in_h.requestFocus();
            return;
        }


        TextView errorText1 = (TextView)spinnerMitarbeiter2.getSelectedView();
        if (spinnerMitarbeiter2Selection == "Select mitarbeiter:") {

            errorText1.setError("anything here, just to add the icon");
            errorText1.setTextColor(Color.RED);
            errorText1.setText("my actual error text");
            return;
        }

        TextView errorText2 = (TextView)spinnerMitarbeiter3.getSelectedView();
        if (spinnerMitarbeiter3Selection == "Select mitarbeiter:") {

            errorText2.setError("anything here, just to add the icon");
            errorText2.setTextColor(Color.RED);
            errorText2.setText("my actual error text");
            return;
        }

        if (n_u12_m.matches("")){n_u12_m_val="0";}else{n_u12_m_val=n_u12_m;}
        if (n_u12_w.matches("")){n_u12_w_val="0";}else{n_u12_w_val=n_u12_w;}
        if (n_u14_m.matches("")){n_u14_m_val="0";}else{n_u14_m_val=n_u14_m;}
        if (n_u14_w.matches("")){n_u14_w_val="0";}else{n_u14_w_val=n_u14_w;}
        if (n_14_17_m.matches("")){n_14_17_m_val="0";}else{n_14_17_m_val=n_14_17_m;}
        if (n_14_17_w.matches("")){n_14_17_w_val="0";}else{n_14_17_w_val=n_14_17_w;}
        if (n_18_20_m.matches("")){n_18_20_m_val="0";}else{n_18_20_m_val=n_18_20_m;}
        if (n_18_20_w.matches("")){n_18_20_w_val="0";}else{n_18_20_w_val=n_18_20_w;}
        if (n_21_24_m.matches("")){n_21_24_m_val="0";}else{n_21_24_m_val=n_21_24_m;}
        if (n_21_24_w.matches("")){n_21_24_w_val="0";}else{n_21_24_w_val=n_21_24_w;}
        if (n_25_26_m.matches("")){n_25_26_m_val="0";}else{n_25_26_m_val=n_25_26_m;}
        if (n_25_26_w.matches("")){n_25_26_w_val="0";}else{n_25_26_w_val=n_25_26_w;}
        if (n_o27_m.matches("")){n_o27_m_val="0";}else{n_o27_m_val=n_o27_m;}
        if (n_o27_w.matches("")){n_o27_w_val="0";}else{n_o27_w_val=n_o27_w;}
        if (anmerkungen.matches("")){anmerkungen_val="-";}else{anmerkungen_val=anmerkungen;}
        if (sonstiges.matches("")){sonstiges_val="-";}else{sonstiges_val=sonstiges;}

        if (datum.matches("\\d{2}.\\d{2}.\\d{4}")){
            datum_val = datum;
        }else{
            et_datum.setError("Please check the date format");
            et_datum.requestFocus();
            return;
        }

        if (th_sucht.isChecked()){th_sucht_val = "1";}else{th_sucht_val = "0";}
        if (th_gewalt.isChecked()){th_gewalt_val = "1";}else{th_gewalt_val = "0";}
        if (th_kriminalitat.isChecked()){th_kriminalitat_val = "1";}else{th_kriminalitat_val = "0";}
        if (th_gesundheit.isChecked()){th_gesundheit_val = "1";}else{th_gesundheit_val = "0";}
        if (th_freizeit.isChecked()){th_freizeit_val = "1";}else{th_freizeit_val = "0";}
        if (th_identitat.isChecked()){th_identitat_val = "1";}else{th_identitat_val = "0";}
        if (th_wohnraum.isChecked()){th_wohnraum_val = "1";}else{th_wohnraum_val = "0";}
        if (th_familie.isChecked()){th_familie_val = "1";}else{th_familie_val = "0";}
        if (th_behorden.isChecked()){th_behorden_val = "1";}else{th_behorden_val = "0";}
        if (th_finanzielle.isChecked()){th_finanzielle_val = "1";}else{th_finanzielle_val = "0";}
        if (th_arbeit.isChecked()){th_arbeit_val = "1";}else{th_arbeit_val = "0";}
        if (th_beziehung.isChecked()){th_beziehung_val = "1";}else{th_beziehung_val = "0";}
        if (th_politische_themen.isChecked()){th_politische_themen_val = "1";}else{th_politische_themen_val = "0";}
        if (th_digitales.isChecked()){th_digitales_val = "1";}else{th_digitales_val = "0";}
        if (th_vorstellung.isChecked()) {th_vorstellung_val = "1";}else{th_vorstellung_val = "0";}

//        int bekannt_sum = Integer.parseInt(n_bekannt_val) + Integer.parseInt(n_unbekannt_val);
//        int people_sum = Integer.parseInt(n_u12_m_val) + Integer.parseInt(n_u12_w_val) + Integer.parseInt(n_u14_m_val) + Integer.parseInt(n_u14_m_val) +
//                Integer.parseInt(n_14_17_m_val) + Integer.parseInt(n_14_17_w_val) + Integer.parseInt(n_18_20_m_val) + Integer.parseInt(n_18_20_m_val) +
//                Integer.parseInt(n_21_24_m_val) + Integer.parseInt(n_21_24_w_val) + Integer.parseInt(n_25_26_m_val) + Integer.parseInt(n_25_26_m_val) +
//                Integer.parseInt(n_o27_m_val) + Integer.parseInt(n_o27_w_val);
//        if (bekannt_sum != people_sum) {
//            et_n_bekannt.setError("");
//            et_n_unbekannt.setError("Number of bekannt and unbekannt should be equal to the totan number of people are met");
//        } else {

            progressDialog.setMessage("Adding new record...");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    Constants.URL_ADD_EINZELFALLHILFE_RECORD,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.hide();
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("datum", datum_val);
                    params.put("dauer_in_h", dauer_in_h);
//                    params.put("n_bekannt", n_bekannt_val);
//                    params.put("n_unbekannt", n_unbekannt_val);
                    params.put("n_u12_m", n_u12_m_val);
                    params.put("n_u12_w", n_u12_w_val);
                    params.put("n_u14_m", n_u14_m_val);
                    params.put("n_u14_w", n_u14_w_val);
                    params.put("n_u14_17_m", n_14_17_m_val);
                    params.put("n_u14_17_w", n_14_17_w_val);
                    params.put("n_u18_20_m", n_18_20_m_val);
                    params.put("n_u18_20_w", n_18_20_w_val);
                    params.put("n_u21_24_m", n_21_24_m_val);
                    params.put("n_u21_24_w", n_21_24_w_val);
                    params.put("n_u25_26_m", n_25_26_m_val);
                    params.put("n_u25_26_w", n_25_26_w_val);
                    params.put("n_o27_m", n_o27_m_val);
                    params.put("n_o27_w", n_o27_w_val);
                    params.put("th_sucht", th_sucht_val);
                    params.put("th_gewalt", th_gewalt_val);
                    params.put("th_kriminalitat", th_kriminalitat_val);
                    params.put("th_gesundheit", th_gesundheit_val);
                    params.put("th_freizeit", th_freizeit_val);
                    params.put("th_identitat", th_identitat_val);
                    params.put("th_wohnraum", th_wohnraum_val);
                    params.put("th_familie", th_familie_val);
                    params.put("th_behorden", th_behorden_val);
                    params.put("th_finanzielle", th_finanzielle_val);
                    params.put("th_arbeit", th_arbeit_val);
                    params.put("th_beziehung", th_beziehung_val);
                    params.put("th_politische_themen", th_politische_themen_val);
                    params.put("th_digitales", th_digitales_val);
                    params.put("th_vorstellung", th_vorstellung_val);
                    params.put("th_sonstiges", sonstiges_val);
                    params.put("anmerkungen", anmerkungen_val);
                    params.put("mitarbeiter01", mitarbeiter1);
                    params.put("mitarbeiter02", spinnerMitarbeiter2Selection);
                    params.put("mitarbeiter03", spinnerMitarbeiter3Selection);
                    params.put("project_name", spinnerProjectNameSelection);
                    return params;
                }
            };
            RequestHandler.getInstance(this).addToRequestQueue(stringRequest);


    }

    private void fetchUsers() {
        progressDialog.setMessage("Filling the selectors...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                Constants.URL_FETCH_USERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            result = jsonObject.getJSONArray("users");
                            empdetailsUsers(result);
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void empdetailsUsers(JSONArray jsonObject) {

        usersList.clear();

        for (int i = 0; i < jsonObject.length(); i++) {
            try {
                JSONObject json = jsonObject.getJSONObject(i);
                usersList.add(json.getString("username"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        usersList.add(0,"Select mitarbeiter:");
        ArrayAdapter<String> usersAdapter = new ArrayAdapter<String>(EinzelfallHilfeActivity.this, android.R.layout.simple_spinner_dropdown_item, usersList){

            @Override
            public boolean isEnabled(int position){
                if (position == 0){
                    return false;
                }else{
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position==0) {
                    // Set the disable item text color
                    tv.setTextColor(Color.BLUE);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return super.getDropDownView(position, convertView, parent);
            }
        };

        usersAdapter.notifyDataSetChanged();
        spinnerMitarbeiter2.setAdapter(usersAdapter);
        spinnerMitarbeiter3.setAdapter(usersAdapter);
//        spinnerUsers.setAdapter(new ArrayAdapter<String>(StreetworkActivityProto.this, android.R.layout.simple_spinner_dropdown_item, usersList));

    }

    private void fetchProjects() {
        progressDialog.setMessage("Filling the selectors...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                Constants.URL_FETCH_PROJECTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            resultProject = jsonObject.getJSONArray("project");
                            empdetailsProjects(resultProject);
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void empdetailsProjects(JSONArray jsonObject) {

        projectsList.clear();

        for (int i = 0; i < jsonObject.length(); i++) {
            try {
                JSONObject json = jsonObject.getJSONObject(i);
                projectsList.add(json.getString("project_name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        projectsList.add(0,"Select project:");
        ArrayAdapter<String> projectsAdapter = new ArrayAdapter<String>(EinzelfallHilfeActivity.this, android.R.layout.simple_spinner_dropdown_item, projectsList){
            @Override
            public boolean isEnabled(int position){
                if (position == 0){
                    return false;
                }else{
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position==0) {
                    // Set the disable item text color
                    tv.setTextColor(Color.BLUE);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return super.getDropDownView(position, convertView, parent);
            }
        };
        projectsAdapter.notifyDataSetChanged();
        spinnerProjectName.setAdapter(projectsAdapter);
    }

    private void removeProject(){
        if (spinnerProjectName.getSelectedItem().toString()=="Select project:"){
            Toast.makeText(getApplicationContext(), "Choose the value to delete", Toast.LENGTH_LONG).show();
        }else {
            progressDialog.setMessage("Deleting selected project");
            progressDialog.show();

            final String spinnerProjectNameSelection = spinnerProjectName.getSelectedItem().toString().trim();
            final String table_name = "project";
            final String field_name = "project_name";

            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    Constants.URL_ITEM_DELETE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.hide();
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("table_name", table_name);
                    params.put("field_name", field_name);
                    params.put("item_name", spinnerProjectNameSelection);
                    return params;
                }
            };
            RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
        }
    }

    private void add_new_user(){
        Intent i = new Intent(getApplicationContext(), UserRegisterActivity.class);
        startActivity(i);
    }

    private void add_new_project(){
        Intent i = new Intent(getApplicationContext(), ProjectAddActivity.class);
        startActivity(i);
    }


    @Override
    public void onClick(View view) {
        if(view == btn_submit){
            submitRecord();
        }
        else if(view == btn_new_record){
//            clearFields();
        }
        else if(view == btn_revert){
//            deleteLastRecord();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menuLogout:
                SharedPrefManager.getInstance(this).logout();
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.menuSettings:
                Toast.makeText(this, "You clicked settings", Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId()==R.id.spinnerProjectName){
            getMenuInflater().inflate(R.menu.menu_spinner_project, menu);
        }

        if (v.getId()==R.id.spinnerMitarbeiter3){
            getMenuInflater().inflate(R.menu.menu_spinner_users, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_user:
                Toast.makeText(getApplicationContext(), "Add user Clicked", Toast.LENGTH_LONG).show();
                add_new_user();
                return true;
            case R.id.remove_user:
                Toast.makeText(getApplicationContext(), "Remove user Clicked", Toast.LENGTH_LONG).show();
//                remove_user();
                return true;
            case R.id.add_project:
                Toast.makeText(getApplicationContext(), "Add project Clicked", Toast.LENGTH_LONG).show();
                add_new_project();
                return true;
            case R.id.remove_project:
                Toast.makeText(getApplicationContext(), "Remove project Clicked", Toast.LENGTH_LONG).show();
                removeProject();
                fetchProjects();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }




}
